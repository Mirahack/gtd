O:Today, I mainly learned the content related to front-end and back-end joint debugging, including solving cross domain problems, etc. We also had our third presentation in the afternoon.

R:I feel Lost but will to fight.

I:The presentation of today's PPT was not satisfactory, and compared to the other groups' presentations, it was much inferior, and there was also a slight setback compared to our previous presentations. After completing the presentation, I can feel the feeling of disappointment spreading among the team. After class, we had a meeting with the project leader. We discussed recent issues in the team and some solutions. After the meeting, we felt motivated. I hope our team can regroup after this defeat and strive to perform outstandingly in the next team task.

D:I hope I can do my best to improve myself and help the team.