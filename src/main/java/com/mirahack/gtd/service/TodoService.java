package com.mirahack.gtd.service;

import com.mirahack.gtd.dto.TodoRequestDTO;
import com.mirahack.gtd.dto.TodoResponseDTO;
import com.mirahack.gtd.entity.Todo;
import com.mirahack.gtd.exception.TodoNotFoundException;
import com.mirahack.gtd.mapper.TodoMapper;
import com.mirahack.gtd.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public List<TodoResponseDTO> list() {
        return todoRepository
                .findAll()
                .stream()
                .map(TodoMapper::convertTodoToTodoResponse)
                .collect(Collectors.toList());
    }

    public TodoResponseDTO get(Integer id) {
        Todo todo = todoRepository.findById(id).orElse(null);
        if (todo == null) {
            throw new TodoNotFoundException();
        }
        return TodoMapper.convertTodoToTodoResponse(todo);
    }

    public TodoResponseDTO insert(TodoRequestDTO todoRequestDTO) {
        return TodoMapper
                .convertTodoToTodoResponse(todoRepository.save(TodoMapper.convertTodoRequestToTodo(todoRequestDTO)));
    }

    public TodoResponseDTO update(Integer id, TodoRequestDTO todoRequestDTO) {
        Todo todo = todoRepository.findById(id).orElse(null);
        if (todo == null) {
            throw new TodoNotFoundException();
        }
        if (todoRequestDTO.getDone() != null) {
            todo.setDone(todoRequestDTO.getDone());
        }
        if (todoRequestDTO.getText() != null) {
            todo.setText(todoRequestDTO.getText());
        }
        todoRepository.save(todo);
        return TodoMapper.convertTodoToTodoResponse(todo);
    }

    public void delete(Integer id) {
        todoRepository.deleteById(id);
    }
}
