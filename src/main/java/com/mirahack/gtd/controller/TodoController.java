package com.mirahack.gtd.controller;

import com.mirahack.gtd.dto.TodoRequestDTO;
import com.mirahack.gtd.dto.TodoResponseDTO;
import com.mirahack.gtd.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping
    public List<TodoResponseDTO> list() {
        return todoService.list();
    }

    @GetMapping("/{id}")
    public TodoResponseDTO get(@PathVariable("id") Integer id) {
        return todoService.get(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponseDTO insert(@RequestBody TodoRequestDTO todoRequestDTO) {
        return todoService.insert(todoRequestDTO);
    }

    @PutMapping("/{id}")
    public TodoResponseDTO update(@PathVariable("id") Integer id, @RequestBody TodoRequestDTO todoRequestDTO) {
        return todoService.update(id, todoRequestDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        todoService.delete(id);
    }

}
