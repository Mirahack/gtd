package com.mirahack.gtd.mapper;

import com.mirahack.gtd.dto.TodoRequestDTO;
import com.mirahack.gtd.dto.TodoResponseDTO;
import com.mirahack.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponseDTO convertTodoToTodoResponse(Todo item) {
        TodoResponseDTO todoResponseDTO = new TodoResponseDTO();
        BeanUtils.copyProperties(item, todoResponseDTO);
        return todoResponseDTO;
    }

    public static Todo convertTodoRequestToTodo(TodoRequestDTO todoRequestDTO) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequestDTO, todo);
        todo.setDone(false);
        return todo;
    }
}
