package com.mirahack.gtd.dto;

import lombok.Data;

@Data
public class TodoResponseDTO {
    private Integer id;
    private String text;
    private Boolean done;
}
