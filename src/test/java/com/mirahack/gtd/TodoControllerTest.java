package com.mirahack.gtd;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mirahack.gtd.dto.TodoRequestDTO;
import com.mirahack.gtd.entity.Todo;
import com.mirahack.gtd.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todo_list_when_get_list_given_repo() throws Exception {
        Todo todo1 = new Todo("do thing 1", false);
        Todo todo2 = new Todo("do thing 2", false);
        todoRepository.saveAll(List.of(todo1, todo2));

        client.perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(todo1.getId()))
                .andExpect(jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(jsonPath("$[0].done").value(false))
                .andExpect(jsonPath("$[1].id").value(todo2.getId()))
                .andExpect(jsonPath("$[1].text").value(todo2.getText()))
                .andExpect(jsonPath("$[1].done").value(false));
    }

    @Test
    void should_return_specific_todo_item_when_get_given_repo() throws Exception {
        Todo todo = new Todo("do some thing", false);
        Todo saveTodo = todoRepository.save(todo);

        client.perform(get("/todos/{id}", saveTodo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(todo.getId()))
                .andExpect(jsonPath("$.text").value(todo.getText()))
                .andExpect(jsonPath("$.done").value(false));
    }

    @Test
    void should_return_saved_item_when_post_given_repo_and_item() throws Exception {
        TodoRequestDTO saveTodoItem = new TodoRequestDTO("save todo item", false);

        client.perform(post("/todos").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(saveTodoItem)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value(saveTodoItem.getText()))
                .andExpect(jsonPath("$.done").value(saveTodoItem.getDone()));

        List<Todo> todos = todoRepository.findAll();
        assertEquals(saveTodoItem.getText(), todos.get(0).getText());
    }

    @Test
    void should_return_updated_item_when_put_done_given_repo_and_item() throws Exception {
        Todo todo1 = new Todo("unfinished todo", true);
        Todo todo2 = new Todo("finished todo", false);
        List<Todo> todoList = todoRepository.saveAll(List.of(todo1, todo2));
        TodoRequestDTO unfinishedTodo = new TodoRequestDTO("unfinished todo", false);
        TodoRequestDTO finishedTodo = new TodoRequestDTO("finished todo", true);

        client.perform(put("/todos/{id}", todoList.get(0).getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(unfinishedTodo)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(unfinishedTodo.getText()))
                .andExpect(jsonPath("$.done").value(unfinishedTodo.getDone()));
        client.perform(put("/todos/{id}", todoList.get(1).getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(finishedTodo)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(finishedTodo.getText()))
                .andExpect(jsonPath("$.done").value(finishedTodo.getDone()));
        List<Todo> todos = todoRepository.findAll();
        assertEquals(todos.get(0).getDone(), unfinishedTodo.getDone());
        assertEquals(todos.get(1).getDone(), finishedTodo.getDone());
    }

    @Test
    void should_return_updated_item_when_put_text_given_repo_and_item() throws Exception {
        Todo todo = new Todo("before update todo", false);
        Todo saveTodo = todoRepository.save(todo);
        TodoRequestDTO todoRequestDTO = new TodoRequestDTO("after update todo", false);

        client.perform(put("/todos/{id}", saveTodo.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todoRequestDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(todoRequestDTO.getText()))
                .andExpect(jsonPath("$.done").value(todoRequestDTO.getDone()));
        List<Todo> todos = todoRepository.findAll();
        assertEquals(todos.get(0).getText(), todoRequestDTO.getText());
    }

    @Test
    void should_return_void_when_delete_and_get_given_repo_and_item() throws Exception {
        Todo todo = new Todo("delete item", false);
        Todo saveTodo = todoRepository.save(todo);

        client.perform(delete("/todos/{id}", saveTodo.getId()))
                .andExpect(status().isNoContent());
        assertNull(todoRepository.findById(saveTodo.getId()).orElse(null));
    }

    @Test
    void should_throw_todo_not_found_exception_when_get_given_repo_and_id() throws Exception {
        client.perform(get("/todos/{id}", 1))
                .andExpect(jsonPath("$.code").value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.message").value("Todo not found."));
    }


}
